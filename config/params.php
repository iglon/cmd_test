<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'api' => [
        'site' => 'https://www.cbr-xml-daily.ru/daily_json.js'
    ]
];
