<?php

namespace app\services;

class CurrencyService {

    /** @var string */
    private $defaultCurrency = 'RUB';

    /** @var int */
    private $defaultRateSum = 1;

    /**
     *  Подсчет курса
     *
     * @param $currency
     * @param bool $rateCurrency
     * @param bool $rateSum - кол-во денег
     * @return array
     */
    public function getCurrencyRate($currency, $rateCurrency, $rateSum) {
        $currentRates = $this->getCurrentRates();

        foreach ($currentRates['Valute'] as $charCode => $currentRate) {
            if ($charCode == $currency) {
                $result = [
                    "name" => $currentRate['Name'],
                    "code" => $currentRate["CharCode"],
                    "rateCurrency" => $rateCurrency ? $rateCurrency : $this->defaultCurrency,
                    "rateSum" => $rateSum ? $rateSum : $this->defaultRateSum,
                    "rate" => $currentRate['Value']
                ];
                //грязный ход: для подсчета общей стоимости
                $result["result"] = $result['rate'] * $result['rateSum'];
            }
        }

        return $result;
    }

    /**
     * Получение актуальных курс валют
     *
     * @return array
     */
    public function getCurrentRates() {
        $curl = curl_init(\Yii::$app->params['api']['site']);
        if ($curl) {
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
            $data = curl_exec($curl);
            curl_close($curl);
            unset($curl);
        }
        return json_decode($data, true);
    }

}