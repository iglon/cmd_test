<?php

namespace app\controllers;

use app\services\CurrencyService;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use Yii;
use yii\web\Response;

class CurrencyController extends Controller {

    /**
     * @param array $params
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionGetCurrencyRate(array $params) {
        if (!key_exists('currency', $params)) {
            throw new BadRequestHttpException('Required parameter \'currency\'');
        }

        $rateCurrency = false;
        $rateSum = false;

        if (key_exists('rateCurrency', $params)) {
            $rateCurrency = $params['rateCurrency'];
        }

        if (key_exists('rateSum', $params)) {
            $rateSum = $params['rateSum'];
        }

        $currencyRate = (new CurrencyService())->getCurrencyRate(
            $params['currency'],
            $rateCurrency,
            $rateSum
        );

        return $currencyRate;
    }

    /**
     * Получение body из post запроса
     *
     * {@inheritDoc}
     */
    public function runAction($id, $params = []) {
        // Extract the params from the request and bind them to params
        $params = \yii\helpers\BaseArrayHelper::merge(Yii::$app->getRequest()->getBodyParams(), $params);
        return parent::runAction($id, $params);
    }

    /**
     * Вывод в формате Json
     *
     * {@inheritDoc}
    */
    public function beforeAction($action) {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }
}
